- [FindMovie](#org6df180c)
  - [Screenshot](#orge67365e)
  - [Install](#orgb679973)
  - [License](#org490c16a)


<a id="org6df180c"></a>

# FindMovie

Findmovie is a simple script to fetch details about movies using IMDB in the terminal.


<a id="orge67365e"></a>

## Screenshot

![img](./Screenshot.png "This is an example of the script")


<a id="orgb679973"></a>

## Install

Add the following to .bashrc

```
export PATH=$PATH:absolute/path/to/bin
```

Replacing it with the path to the script.


<a id="org490c16a"></a>

## License

GPL v3 - <https://www.gnu.org/licenses/gpl-3.0.en.html>